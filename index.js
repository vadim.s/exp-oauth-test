const express = require('express');
const path = require('path');

const app = express();
const cors = require('cors');
const passport = require('passport');
const cookieSession = require('cookie-session');
const jwt = require('./jwt');
const sendMail = require('./mail');
const sequelize = require('./db/dbConnect');
const User = require('./models/User');

require('./passport-setup');

app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(cookieSession({
  name: 'gauth-session',
  keys: ['key1', 'key2'],
}));

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

const isLoggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.sendStatus(401);
  }
};

app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => res.render('index'));
app.get('/failed', (req, res) => res.send('You failed to log in'));
app.get('/good', isLoggedIn, (req, res) => res.render('good', { name: req.user.displayName }));
app.get('/link-ok', (req, res) => res.send('Success'));

app.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
app.get('/facebook', passport.authenticate('facebook'));

app.get('/google/callback', passport.authenticate('google', { failureRedirect: '/failed' }),
  (req, res) => {
    res.redirect('/good');
  });

app.get('/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/failed' }),
  (req, res) => {
    res.redirect('/good');
  });

app.get('/logout', (req, res) => {
  req.session = null;
  req.logout();
  res.redirect('/');
});

app.post('/magiclink', (req, res) => {
  const { email } = req.body;
  const token = jwt.generateToken(email);

  if (email && token) {
    sendMail(email, token)
      .then((result) => {
        console.log('Success', result);
        res.status(200).json({ message: 'Email sent' });
      })
      .catch((error) => res.status(500).json({ message: 'Error', error }));
  } else {
    res.status(500).json({ message: 'Missed email or token' });
  }
});

app.get('/account', async (req, res) => {
  const { token } = req.query;

  try {
    const data = await jwt.verifyToken(token);
    // console.log(data);
    const { expiration, email } = data;
    if (Date.parse(expiration) > Date.now()) {
      await User.sync({ force: true }); // creates or re-creates table (only for testing)
      const savedUser = await User.create({ email });
      console.log('This is auto-generated ID:', savedUser.id);
      res.redirect('/link-ok');
    } else {
      console.log('Token expired');
      res.redirect('/failed');
    }
  } catch (err) {
    console.log(err);
    res.redirect('/failed');
  }
});

// Test database connection
sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

app.listen(3000, () => console.log('App listening on port 3000'));
