const form = document.querySelector('form');

form.addEventListener('submit', async (event) => {
  event.preventDefault();

  const { action, method } = event.target;
  const email = event.target.email.value;

  const response = await fetch(action, {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email }),
  });

  if (response.ok) {
    alert('Успешно! Проверьте почту');
  } else {
    const data = await response.json();
    console.log(data);
    console.log(`Возникла ошибка: ${data.error}`);
  }
});
