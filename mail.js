const nodemailer = require('nodemailer');
require('dotenv').config();

const transporter = nodemailer.createTransport({
  service: 'Yandex',
  auth: {
    user: process.env.EMAIL_LOGIN,
    pass: process.env.EMAIL_PASSWORD,
  },
});

const sendMail = async (email, token) => {
  const link = `${process.env.HOST}/account?token=${token}`;

  const message = {
    from: process.env.EMAIL_LOGIN,
    to: email,
    subject: 'Nodemailer test',
    text: `Here is your link: ${link}`,
    html: `<p>Here is your link:</p><p>${link}</p>`,
  };

  await transporter.sendMail(message);
};

module.exports = sendMail;
