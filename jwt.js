const jwt = require('jsonwebtoken');
require('dotenv').config();

const generateToken = (email) => {
  const date = new Date();
  date.setHours(date.getHours() + 1);
  return jwt.sign({ email, expiration: date }, process.env.JWT_SECRET);
};

const verifyToken = async (token) => jwt.verify(token, process.env.JWT_SECRET);

module.exports = { generateToken, verifyToken };
