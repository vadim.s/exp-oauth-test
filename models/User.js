const { DataTypes } = require('sequelize');
const sequelize = require('../db/dbConnect');

const User = sequelize.define('User', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  email: { type: DataTypes.STRING, allowNull: false, unique: true },
});

module.exports = User;
