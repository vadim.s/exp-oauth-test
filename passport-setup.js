const passport = require('passport');
require('dotenv').config();

const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

// passport.serializeUser(function(user, done) {
//   done(null, user.id);
// });
passport.serializeUser((user, done) => {
  console.log('serializeUser', user);
  done(null, user);
});

// passport.deserializeUser(function(id, done) {
//   User.findById(id, function(err, user) {
//     done(err, user);
//   });
// });
passport.deserializeUser((user, done) => {
  console.log('deserializeUser', user);
  done(null, user);
});

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: '/google/callback',
}, ((accessToken, refreshToken, profile, done) =>
  //  use the profile info (mainly profile id to check if the user is registered in your db)
  //  User.findOrCreate({ googleId: profile.id }, (err, user) => done(err, user));
  done(null, profile)
)));

passport.use(new FacebookStrategy({
  clientID: process.env.FACEBOOK_APP_ID,
  clientSecret: process.env.FACEBOOK_APP_SECRET,
  callbackURL: '/facebook/callback',
},
((accessToken, refreshToken, profile, done) => done(null, profile))));
