const { Sequelize } = require('sequelize');
require('dotenv').config();

const sequelize = new Sequelize(process.env.PSQL_CONN_STRING);

module.exports = sequelize;
